import { Injectable, HttpService } from "@nestjs/common";
import * as sgMail from "@sendgrid/mail";
import * as functions from "firebase-functions";

@Injectable()
export class AppEmailService {
  private readonly config = functions.config().config

  constructor(private httpService: HttpService) {
    sgMail.setApiKey(this.config.smtp);
  }

  public async sendEmail(body: any, ip: string) {
    // finish recaptcha
    const validate = await this.httpService
      .post(
        `https://www.google.com/recaptcha/api/siteverify?response=${body.recaptchaValue}&secret=${this.config.recaptcha}&remoteip=${ip}`
      )
      .toPromise();

    if (!validate.data.success) {
      console.log("Failed recaptcha:", validate.data);
      return new Error("Recaptcha failed.");
    }

    await sgMail.send({
      to: "info@datails.nl",
      from: "datails.smtp@gmail.com",
      subject: "Website contact datails.nl",
      templateId: "d-c7b03f5661ed4c9aaafcb27395aa4fdf",
      dynamicTemplateData: {
        email: body.email,
        name: body.naam,
        body: body.message,
      },
    });
  }
}
