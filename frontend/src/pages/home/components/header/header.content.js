export default {
    title: 'Freelance Senior Software Engineer',
    image: require('../../../../assets/logo-header.png'),
    subtitle: 'DATAILS.',
    desc: `Senior Software Engineer specialized in NodeJS, Go and Python. Proficient in AWS, GCP, Docker/Kubernetes, Linux/Bash, AWS/GCP, CI/CD (GitLab/GitHub), CLI development and databases (SQL/NoSQL). `,
}