export { default as AppHome } from "./home";
export { default as App404 } from "./404";
export { default as AppAbout } from "./about";
export { default as AppContact } from "./contact";
export { default as AppSideProjects } from "./side-projects";
export { default as AppVolunteering } from "./volunteering"; 